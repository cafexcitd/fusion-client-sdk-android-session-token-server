<?php

    // extract the session ID from the HTTP GET parameter
    $id = urlencode($_GET['id']);
    
    // send an HTTP DELETE to the gateway
    $ch = curl_init("http://fusion.vbox:8080/gateway/sessions/session/id/$id");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     // don't print output
    curl_exec($ch);
    
    // print the result of the operation
    print curl_getinfo($ch, CURLINFO_HTTP_CODE);
?>