<?php
    // include our provisioning script
    require_once('init.php');

    // provision a session using params from the query string
    $token = provision($_GET['username'], $_GET['password']);
    
    // decode the response and print the result into the HTTP response
    $decoded = json_decode($token);
    echo $decoded->sessionid;
?>